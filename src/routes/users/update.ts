import { FastifyInstance, FastifyReply, FastifyRequest } from 'fastify';
import S from 'fluent-json-schema';
import { Knex } from 'knex';

import { User } from '../../models/user';

export default async (fastify: FastifyInstance): Promise<void> => {
    const db = fastify.db as Knex;
    const user = new User();
    // Set database connection
    user.setConnection(db);

    /**
     * User registration
     * @param id User id
     * @param fname First name
     * @param lname Last name
     */
    fastify.put(
        '/:id/update',
        {
            schema: {
                body: S.object()
                    .prop('fname', S.string().maxLength(20).minLength(4))
                    .prop('lname', S.string().maxLength(20).minLength(4)),
                params: S.object().prop('id', S.string().format('uuid')),
            },
        },
        async function (request: FastifyRequest, reply: FastifyReply) {
            const { fname, lname } = request.body as {
                fname: string;
                lname: string;
            };

            const { id } = request.params as { id: string };

            try {
                user.userId = id;
                user.fname = fname;
                user.lname = lname;
                // Update user
                await user.update();

                // Response to client
                return reply.status(200).send({ ok: true });
            } catch (error) {
                // Set logging
                request.log.error({
                    message: error,
                    reqId: request.id,
                    module: 'USER',
                });
                return reply.status(500).send({ ok: false });
            }
        }
    );
};
