import { FastifyInstance } from 'fastify';

export default async (fastify: FastifyInstance): Promise<void> => {
    fastify.get('/logout', async function () {
        return 'Logout...';
    });
};
