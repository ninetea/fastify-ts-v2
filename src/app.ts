import { FastifyPluginAsync, FastifyServerOptions } from 'fastify';
import { join } from 'path';

import AutoLoad, { AutoloadPluginOptions } from '@fastify/autoload';

export interface AppOptions extends FastifyServerOptions, Partial<AutoloadPluginOptions> {}

const options: AppOptions = {};

const app: FastifyPluginAsync<AppOptions> = async (fastify, opts): Promise<void> => {
    // Place here your custom code!

    // Do not touch the following lines
    void fastify.register(AutoLoad, {
        dir: join(__dirname, 'plugins'),
        options: opts,
    });

    void fastify.register(AutoLoad, {
        dir: join(__dirname, 'routes'),
        dirNameRoutePrefix: true,
        options: opts,
    });
};

export default app;
export { app, options };
