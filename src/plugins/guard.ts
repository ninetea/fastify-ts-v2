import fp from 'fastify-plugin';

const fastifyGuard = require('fastify-guard');

export default fp(async (fastify) => {
    fastify.register(fastifyGuard, {
        errorHandler: (_result: any, _request: any, reply: any) => {
            return reply.methodNotAllowed();
        },
    });
});
