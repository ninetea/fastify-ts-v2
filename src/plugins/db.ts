import fp from 'fastify-plugin';
import Knex from 'knex';

export default fp(async (fastify) => {
    const handler = Knex({
        client: 'pg',
        connection: {
            host: process.env.DB_HOST,
            port: Number(process.env.DB_PORT || '5432'),
            user: process.env.DB_USER,
            password: process.env.DB_PASSWORD,
            database: process.env.DB_NAME,
        },
        pool: {
            min: 2,
            max: Number(process.env.DB_POOL_MAX || '10'),
        },
        debug: process.env.DB_DEBUG === 'Y',
    });

    fastify.decorate('db', handler);
    fastify.addHook('onClose', (fastify, done) => {
        fastify.db.destroy(() => {
            done();
        });
    });
});

declare module 'fastify' {
    export interface FastifyInstance {
        db: any;
    }
}
