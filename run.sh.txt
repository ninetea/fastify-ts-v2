NODE_ENV=developement \
DB_HOST=127.0.0.1 \
DB_PORT=5432 \
DB_USER=postgres \
DB_PASSWORD=xxxxxx \
DB_NAME=test \
DB_POOL_MAX=10 \
DB_DEBUG=Y \
JWT_ISSUER=dev.my.local \
JWT_EXPIRES_IN=36000000 \
MAX_RATE_LIMIT=100 \
UPLOAD_PATH=./uploads \
TMP_PATH=./tmp \
npm run dev