# Fastify Template with Typescript

## Template

Routing template

```typescript
import { FastifyInstance, FastifyReply, FastifyRequest } from 'fastify';

export default async (fastify: FastifyInstance): Promise<void> => {
    fastify.get('/', async function (request: FastifyRequest, reply: FastifyReply) {
        reply.send({ ok: true });
    });
};
```

Import routings

```typescript
import { FastifyInstance } from 'fastify';

import other from './other';

export default async (fastify: FastifyInstance): Promise<void> => {
    // Register routes
    fastify.register(other);
};
```

Model

```typescript
import { Knex } from 'knex';

export class User {
    private db!: Knex;

    setDb(db: Knex) {
        this.db = db;
    }

    /**
     * User login
     * @param username
     */
    check(username: string): Promise<IUser> {
        return this.db('users').where('username', username).select('user_id', 'password').first();
    }
}
```

Using model

```typescript
const db = fastify.db as Knex;
const user = new User();

user.setDb(db);
const info = await user.check('satit');
```

## Generate JWT RSA

```shell
ssh-keygen -t rsa -b 1024 -m PEM -f certs/rsa.key
openssl rsa -in certs/rsa.key -pubout -outform PEM -out certs/rsa.key.pub
```
